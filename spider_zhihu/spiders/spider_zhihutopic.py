# -*- coding: utf-8 -*-
import json

import scrapy
import time

from spider_zhihu.proxy.HeaderUtil import HeaderUtil
from spider_zhihu.service.login_service import LoginService
from spider_zhihu.service.topic_service import TopicService
from spider_zhihu.service.answer_info_service import AnswerInfoService
from spider_zhihu.service.article_info_service import ArticleInfoService
from spider_zhihu.service.article_data_service import ArticleDataStateJsonService
from spider_zhihu.spider_property import SpiderProperty

'''
知乎文章爬虫
作者：O2LEE
时间：2017-11-23

设置：scrapy默认把Request的cookie放进去，cookie不需要显示使用cookiejar设置
'''


class SpiderZhihutopicSpider(scrapy.Spider):
    name = "spider_zhihutopic"
    allowed_domains = ["zhihu.com"]
    allowed_domains = ["www.zhihu.com"]
    need_login = False
    x_xsrf_token = None

    def start_requests(self):
        if self.need_login:
            # 为了提取_xsrf：要先访问知乎的登录页面，让scrapy在登录页面获取服务器给我们的数据(_xsrf)，再调用do_login
            return [scrapy.Request(SpiderProperty.signin_page_url, headers=HeaderUtil.get_login_request_header(),
                                   callback=self.do_login)]
        else:
            # 暂不登录，直接访问话题广场
            return [scrapy.Request(SpiderProperty.topic_type_url, headers=HeaderUtil.get_common_request_header(),
                                   callback=self.analyse_topic_type)]

    '''
    一、分析登录页面，获取_xsrf信息，并且发送验证码图片请求，将请求交与方法：analyse_captcha进一步处理
    '''

    def do_login(self, response):
        # 参数_xsrf值
        xsrf_token = LoginService.get_xsrf_token(response)

        # 如果提取到了xsrf_token值，就进行下面的操作，如果没xsrf有就没必要往下做了
        if xsrf_token and len(xsrf_token) > 0:
            self.x_xsrf_token = xsrf_token
            post_data = LoginService.compose_login_data("cn", xsrf_token, "***********", "********", "")
            url = SpiderProperty.captcha_url + '&r=' + str(int(time.time() * 1000))
            return scrapy.Request(url, meta={'post_data': post_data}, headers=HeaderUtil.get_login_request_header(),
                                  callback=self.analyse_captcha)
        else:
            print(">>>>>>>>>>>>>似乎没有成功获取到_xsrf的值，无法进行认证登录操作！")

    '''
    二、获取验证码图片，保存到本地，并且完成输入后发送登录请求，将请求交与方法check_login进一步处理
    '''

    def analyse_captcha(self, response):
        # 保存验证码
        captcha_path = LoginService.write_captcha('captcha.gif', response.body)
        # 为了好看，做了点分隔线
        LoginService.print_lines(captcha_path)
        # 等待用户看图后输入结果
        input_numbers = input('请输入您看到的验证码中倒立的文字位置(数字)：\n>')
        # 根据用户的输入获取用户选择的坐标值
        captcha = LoginService.get_captcha_choose(input_numbers)
        # 向试先设置的post_data中添加验证码识别结果
        post_data = LoginService.set_login_data(response, "post_data", "captcha", captcha)
        print('>>>>>>>>>>>>>稍安勿躁，【阿爬】正在努力帮您登录知乎......')
        return [
            scrapy.FormRequest(url=SpiderProperty.login_post_url, formdata=post_data,
                               headers=HeaderUtil.get_login_request_header(),
                               callback=self.request_topic, )]

    '''
    三、验证服务器的返回，如果返回JSON数据，并且消息为‘登录成功’那么表示已经登录成功
       进一步可以向话题广场页面发送请求
    '''

    def request_topic(self, response):
        # 验证服务器的返回数据判断是否成功
        is_login_success = LoginService.is_login_success(response)
        if is_login_success:
            print('>>>>>>>>>>>>>恭喜您！登录成功，【阿爬】马上开始尝试访问话题广场，请稍候......')
            return [scrapy.Request(SpiderProperty.topic_type_url, headers=HeaderUtil.get_common_request_header(),
                                   callback=self.analyse_topic_type)]
        else:
            print('>>>>>>>>>>>>>登录失败')

    '''
    四、分析话题广场页面，获取话题分类列表，offset参数控制
    '''

    def analyse_topic_type(self, response):
        print('>>>>>>>>>>>>>话题广场页面已经拿 到了，【阿爬】正在帮您分析话题类别信息......')
        li_type_list = response.xpath(".//ul[@class='zm-topic-cat-main clearfix']/li");

        if li_type_list and len(li_type_list) > 0:
            for li in li_type_list:
                # 根据指定的HTML元素节点，获取信息组织一个item(类：SpiderTopicTypeItem)
                item = TopicService.compose_topic_type_item(li)
                if item and item["name"] and len(item["name"]) > 0:
                    yield item
                    print(">>>>>>>>>>>>>别急别急，【阿爬】继续请求[" + item["name"][0] + "]的话题列表:")
                    for i in range(0, SpiderProperty.topic_max_page):
                        post_data = TopicService.compose_post_data(i, item["id"][0])
                        yield scrapy.FormRequest(url=SpiderProperty.topics_url,
                                                 headers=HeaderUtil.get_topic_request_header(self.x_xsrf_token),
                                                 formdata=post_data,
                                                 callback=self.analyse_topics,
                                                 meta={"topic_type_id": item["id"]}
                                                 )

    '''
    五、分析返回的话题列表信息，获取话题信息列表
    '''

    def analyse_topics(self, response):
        topic_type_id = response.meta['topic_type_id']
        # print('>>>>>>>>>>>>>【阿爬】开始分析刚刚获取到的话题详细信息列表......')
        response_json = json.loads(response.body_as_unicode())
        if response_json and len(response_json["msg"]) > 0:
            xpath = ".//div[@class='item']/div/a[@target='_blank']"
            topic_item_a_list = scrapy.Selector(text="".join(response_json["msg"])).xpath(xpath)
            item_array = TopicService.list_topic_items(topic_type_id, topic_item_a_list)
            if item_array and len(item_array) > 0:
                for item in item_array:
                    if item["name"] and item["id"]:
                        yield item

                        # print(">>>>>>>>>>>>>那【阿爬】就帮您看看[" + item["name"][0] + "]里有些什么文章可以看吧......")
                        topic_article_list_url = SpiderProperty.topic_hot_url.replace("{toipc_id}", item["id"])
                        # print('>>>>>>>>>>>>>第1页URL：' + topic_article_list_url)

                        # 第一页内容获取是直接get请求，下一页是改为post，url地址不变，需要传前一页最后一条的data-score值为定位参数
                        yield scrapy.Request(topic_article_list_url,
                                             headers=HeaderUtil.get_common_request_header(),
                                             callback=self.analyse_art_list_fpage,
                                             meta={'topic_article_list_url': topic_article_list_url,
                                                   "topic_id": item["id"]})

    '''
    六、获取话题文章列表中的第一页，第一页内容获取是直接get请求
    '''

    def analyse_art_list_fpage(self, response):
        topic_article_list_url = response.meta['topic_article_list_url']
        topic_id = response.meta['topic_id']
        # print('>>>>>>>>>>>>>第1页：【阿爬】开始分析话题中第1页的文章信息列表......')
        xpath = ".//div[@class='zu-top-feed-list']/div[@class='feed-item feed-item-hook  folding']"
        topic_feed_list = response.xpath(xpath)
        item_array = ArticleInfoService.compose_article_from_feed_list(topic_feed_list)
        if item_array and len(item_array) > 0:
            last_data_score = None
            for item in item_array:
                if item["href"] and len(item["href"]) > 0:
                    # 查看这个文章是否已经存在了，如果已经存在了就不爬了
                    if ArticleInfoService.exists(item["id"]):
                        print('>>>>>>>>>>>>>这篇文章看上去好熟悉，算了先不爬它了!')
                    else:
                        url = SpiderProperty.artic_question_url + item["href"][0]
                        # print('>>>>>>>>>>>>>准备访问具体文章信息，URL:' + url)
                        yield scrapy.Request(url, callback=self.analyse_article,
                                             headers=HeaderUtil.get_common_request_header(),
                                             meta={'article_url': url,
                                                   'article_id': item["id"], 'topic_id': topic_id, })
                if item["data_score"] and len(item["data_score"]) > 0:
                    last_data_score = item["data_score"][0]
            if SpiderProperty.article_max_page > 1:
                post_data = ArticleInfoService.compose_article_list_post_data(last_data_score)
                yield scrapy.FormRequest(url=topic_article_list_url, formdata=post_data,
                                         headers=HeaderUtil.get_common_request_header(),
                                         meta={'topic_article_list_url': topic_article_list_url, 'page_num': 2,
                                               'topic_id': topic_id, },
                                         callback=self.analyse_art_list_nextpage)
            else:
                print('>>>>>>>>>>>>>只爬第1页，不需要继续爬取下一页')

    '''
    七、获取话题文章列表中的下一页
    下一页是改为post，url地址不变，需要传当前这一页最后一条的data-score值为定位参数
    '''

    def analyse_art_list_nextpage(self, response):
        topic_article_list_url = response.meta['topic_article_list_url']
        topic_id = response.meta['topic_id']
        page_num = response.meta['page_num']

        print('>>>>>>>>>>>>>第' + str(page_num) + '页：【阿爬】开始分析话题中第' + str(page_num) + '页的文章信息列表......')
        item_html = ""
        response_json = json.loads(response.body_as_unicode())
        if response_json and len(response_json["msg"]) > 0:
            item_html = ArticleInfoService.join_array_to_string(response_json["msg"])

        xpath = ".//div[@class='feed-item feed-item-hook  folding']"
        topic_feed_list = scrapy.Selector(text=item_html).xpath(xpath)
        item_array = ArticleInfoService.compose_article_from_feed_list(topic_feed_list)
        if item_array and len(item_array) > 0:
            last_data_score = None
            for item in item_array:
                if item["href"] and len(item["href"]) > 0:
                    url = SpiderProperty.artic_question_url + item["href"][0]
                    # print('>>>>>>>>>>>>>准备访问具体文章信息，URL:' + url)
                    yield scrapy.Request(url, callback=self.analyse_article,
                                         headers=HeaderUtil.get_common_request_header(),
                                         meta={'article_url': url,
                                               'article_id': item["id"], 'topic_id': topic_id, })
                if item["data_score"] and len(item["data_score"]) > 0:
                    last_data_score = item["data_score"][0]

            # 循环访问N页进行文章列表提取
            if int(page_num) < SpiderProperty.article_max_page:
                post_data = ArticleInfoService.compose_article_list_post_data(last_data_score)
                yield scrapy.FormRequest(url=topic_article_list_url, formdata=post_data,
                                         headers=HeaderUtil.get_common_request_header(),
                                         meta={'topic_article_list_url': topic_article_list_url,
                                               'page_num': (int(page_num) + 1),
                                               'topic_id': topic_id},
                                         callback=self.analyse_art_list_nextpage)
            else:
                print('>>>>>>>>>>>>>这个话题的页面码够了，暂时不爬了，找别的话题去。')

    '''
    八、文章详细内容以及评论页面基础上都是JSON数据操作，从JSON里找内容，从JSON里获取下一页地址
    div[@id='data']/@data-state
    '''

    def analyse_article(self, response):
        article_url = response.meta['article_url']
        article_id = response.meta['article_id']
        topic_id = response.meta['topic_id']
        # print('>>>>>>>>>>>>>【阿爬】开始分析文章的详细内容......')
        data_state = response.xpath(".//div[@id='data']/@data-state").extract()
        if data_state and len(data_state) > 0:
            response_json = ArticleDataStateJsonService.parse_to_json(data_state[0])
            # 从json数据中组织一个文章的内容，返回item(类名：SpiderArticleItem)
            item = ArticleInfoService.compose_article_from_data_state(topic_id, article_id, response_json)
            # yield item到pipelines里去处理，持久化
            yield item

            # 继续处理这篇文章上的回复信息内容
            answer_item_array = AnswerInfoService.compose_answers_from_data_state(article_id, response_json)
            if answer_item_array and len(answer_item_array) > 0:
                for answer_item in answer_item_array:
                    yield answer_item

            if SpiderProperty.reply_max_page > 1:
                # 下一页回答
                # print('>>>>>>>>>>>>>【阿爬】下一页回答内容......')
                next_answer_paget_url = ArticleDataStateJsonService.get_answer_next_page_url(article_id, response_json)
                if next_answer_paget_url:
                    # https://www.zhihu.com/question/68053640
                    yield scrapy.Request(next_answer_paget_url, callback=self.analyse_answer_next_page,
                                         headers=HeaderUtil.get_answer_request_header(article_url),
                                         meta={'article_id': article_id, 'page_num': 2, })

    '''
    九、获取并且分析下一页评论，如果data有数据，一直循环递归
    '''

    def analyse_answer_next_page(self, response):
        page_num = response.meta['page_num']
        article_id = response.meta['article_id']
        # print('>>>>>>>>>>>>>【阿爬】开始分析新一页回复的详细内容......')
        response_json = json.loads(response.body_as_unicode())
        if response_json and response_json["data"] and len(response_json["data"]) > 0:
            answer_item_array = AnswerInfoService.compose_answers_from_json_list(article_id, response_json["data"])
            if answer_item_array and len(answer_item_array) > 0:
                for answer_item in answer_item_array:
                    yield answer_item
            if page_num < SpiderProperty.reply_max_page:
                next_answer_paget_url = AnswerInfoService.get_next_url_from_paging(response_json["paging"])
                if next_answer_paget_url:
                    yield scrapy.Request(next_answer_paget_url, callback=self.analyse_answer_next_page,
                                         headers=HeaderUtil.get_common_request_header(),
                                         meta={'article_id': article_id, 'page_num': (page_num + 1), })
