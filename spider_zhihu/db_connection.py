# -*- coding: utf-8 -*-
import pymysql

from spider_zhihu.dbm import DbManager


class DbConnection:

    @classmethod
    def open(cls):
        host = DbManager.config["host"]
        user = DbManager.config["user"]
        password = DbManager.config["passwd"]
        port = DbManager.config["port"]
        database = DbManager.config["db"]
        charset = DbManager.config["charset"]
        try:
            conn = pymysql.connect(host=host, user=user, passwd=password, db=database, port=port, charset=charset)
            return conn
        except Exception as e:
            print('connect fails!{}'.format(e))

    @classmethod
    def close(cls, cur, conn):
        if cur:
            try:
                cur.close()
            except Exception as e:
                print('cur.close fails!{}'.format(e))
        if conn:
            try:
                conn.close()
            except Exception as e:
                print('conn.close fails!{}'.format(e))
