# -*- coding: utf-8 -*-
from spider_zhihu.db_connection import DbConnection


class TopicInfoDao:

    def __init__(self):
        pass

    '''
    保存话题信息
    '''

    @classmethod
    def save(cls, xid, name, type_id, href):
        conn = None
        cur = None
        try:
            conn = DbConnection.open()
            cur = conn.cursor()
            sql_query = "SELECT * FROM zhihu.topic_info WHERE id = '" + xid + "'"
            cur.execute(sql_query)
            res = cur.fetchall()
            if res and len(res):
                # print("数据已经存在，ID=" + xid + ", NAME:" + name)
                pass
            else:
                sql_excute = "INSERT INTO zhihu.topic_info (Id, name, type_id, href) VALUES  (" + xid + ",'" + name \
                             + "','" + type_id + "','" + href + "')"
                cur.execute(sql_excute)
                conn.commit()
        except Exception as e:
            print(e)
        finally:
            DbConnection.close(cur, conn)
