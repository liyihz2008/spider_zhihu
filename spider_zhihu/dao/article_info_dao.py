# -*- coding: utf-8 -*-

from spider_zhihu.db_connection import DbConnection
from spider_zhihu.items import SpiderArticleItem
from spider_zhihu.service.article_data_service import ArticleDataStateJsonService


class ArticleInfoDao:

    def __init__(self):
        pass

    '''
    保存文章信息
    '''

    @classmethod
    def save(cls, xid, title, topic_id, visit_count, follower_count, answer_count, author, content, updated_time):
        conn = None
        cur = None
        try:
            conn = DbConnection.open()
            cur = conn.cursor()
            sql_query = "SELECT * FROM zhihu.article_info WHERE id = " + str(xid)
            cur.execute(sql_query)
            res = cur.fetchall()
            if res and len(res):
                # print("数据已经存在，ID=" + str(xid) + ", TITLE:" + title)
                pass
            else:
                updated_date = ArticleDataStateJsonService.tran_string_to_date(updated_time)
                sql_excute = "INSERT INTO zhihu.article_info ( Id, topic_id, title, visit_count, follower_count, " \
                             "answer_count, author, updated_time) VALUES  (" + str(xid) + ",'" + str(topic_id) + "','" \
                             + title + "'," + str(visit_count) + "," + str(follower_count) + "," + str(answer_count) \
                             + ",'" + author + "','" + str(updated_date) + ".000')"
                cur.execute(sql_excute)

            sql_query = "SELECT * FROM zhihu.article_detail WHERE id = " + str(xid)
            cur.execute(sql_query)
            res = cur.fetchall()
            if res and len(res):
                # print("文章内容数据已经存在，ID=" + str(xid) + ", TITLE:" + title)
                pass
            else:
                content = ArticleDataStateJsonService.tran_tag_in_content(content)
                sql_excute = "INSERT INTO zhihu.article_detail ( Id, content) VALUES  (" + str(xid) + ",'" + content + "')"
                cur.execute(sql_excute)
            conn.commit()
        except Exception as e:
            print(e)
        finally:
            DbConnection.close(cur, conn)

    '''
    根据ID获取文章信息
    '''

    @classmethod
    def get_article_info(cls, xid):
        conn = None
        cur = None
        try:
            conn = DbConnection.open()
            cur = conn.cursor()
            sql_query = "SELECT ID, TOPIC_ID, TITLE, VISIT_COUNT, FOLLOWER_COUNT, ANSWER_COUNT, AUTHOR, UPDATED_TIME " \
                        "FROM zhihu.article_info WHERE id = " + str(xid)
            cur.execute(sql_query)
            res = cur.fetchone()
            if res:
                article = SpiderArticleItem()
                article["id"] = res[0]
                article["topic_id"] = res[1]
                article["title"] = res[2]
                article["visit_count"] = res[3]
                article["follower_count"] = res[4]
                article["answer_count"] = res[5]
                article["author"] = res[6]
                article["updated_time"] = res[7]
                return article
            return None
        except Exception as e:
            print(e)
        finally:
            DbConnection.close(cur, conn)