# -*- coding: utf-8 -*-

from spider_zhihu.db_connection import DbConnection
from spider_zhihu.service.answer_data_service import AnswerDataStateJsonService


class ReplyInfoDao:

    def __init__(self):
        pass

    '''
    保存回答信息
    '''

    @classmethod
    def save(cls, xid, article_id, comment_count, author, update_time, voteup_count, content):
        conn = None
        cur = None
        try:
            conn = DbConnection.open()
            cur = conn.cursor()
            sql_query = "SELECT * FROM zhihu.reply_info WHERE id = " + str(xid)
            cur.execute(sql_query)
            res = cur.fetchall()
            if res and len(res):
                # print("回答数据已经存在，ID=" + str(xid) + ", AUTHOR:" + author)
                pass
            else:
                updated_date = AnswerDataStateJsonService.tran_string_to_date(update_time)
                sql_excute = "INSERT INTO zhihu.reply_info ( Id, article_id, comment_count, author, updated_time, " \
                             "voteup_count) VALUES  (" + str(xid) + ",'" + str(article_id) + "'," + str(comment_count) + ",'" \
                             + author + "','" + str(updated_date) + ".000'," + str(voteup_count) + " )"
                cur.execute(sql_excute)

            sql_query = "SELECT * FROM zhihu.reply_detail WHERE id = " + str(xid)
            cur.execute(sql_query)
            res = cur.fetchall()
            if res and len(res):
                # print("回答内容数据已经存在，ID=" + str(xid) + ", AUTHOR:" + author)
                pass
            else:
                content = AnswerDataStateJsonService.tran_tag_in_content(content)
                sql_excute = "INSERT INTO zhihu.reply_detail ( Id, content) VALUES  (" + str(xid) + ",'" + content + "')"
                cur.execute(sql_excute)
            conn.commit()
        except Exception as e:
            print(e)
        finally:
            DbConnection.close(cur, conn)
