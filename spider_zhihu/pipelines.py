# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import types

import datetime
import time
from spider_zhihu.dao.article_info_dao import ArticleInfoDao
from spider_zhihu.dao.reply_info_dao import ReplyInfoDao
from spider_zhihu.dao.topic_info_dao import TopicInfoDao
from spider_zhihu.dao.topic_type_dao import TopicTypeDao
from spider_zhihu.items import SpiderTopicTypeItem
from spider_zhihu.items import SpiderTopicItem
from spider_zhihu.items import SpiderArticleItem
from spider_zhihu.items import SpiderReplyItem


class SpiderZhihuPipeline(object):

    def process_item(self, item, spider):
        if isinstance(item, SpiderTopicTypeItem):
            # 保存话题类别信息
            xid, name = None, None
            if item["id"] and len(item["id"]) > 0:
                xid = item["id"][0]
            else:
                xid = "0000000000"
            if item["name"] and len(item["name"]) > 0:
                name = item["name"][0]
            else:
                name = "未知"
            print(">>>>>>>>>>>>>>持久化话题类别：[ID=" + xid + ", NAME=" + name + "]")
            TopicTypeDao.save(xid, name)
        elif isinstance(item, SpiderTopicItem):
            # 保存话题信息
            xid, href, name, type_id = None, None, None, None
            if item["href"] and len(item["href"]) > 0:
                href = item["href"][0]
                xid = href.replace("/topic/","")
            else:
                xid = "0000000000"
                href = "未知"
            if item["name"] and len(item["name"]) > 0:
                name = item["name"][0]
            else:
                name = "未知"
            if item["type_id"] and len(item["type_id"]) > 0:
                type_id = item["type_id"][0]
            else:
                type_id = "未知"
            print(">>>>>>>>>>>>>>持久化话题：[ID=" + xid + ", NAME=" + name + ", HREF=" + href + "]")
            TopicInfoDao.save(xid, name, type_id, href)
        elif isinstance(item, SpiderArticleItem):
            # 保存文章信息
            xid, title, topic_id, visit_count, follower_count = None, None, None, None, None
            answer_count, author, content, updated_time = None, None, None, None
            if item["id"]:
                xid = item["id"]
            else:
                xid = "0000000000"
            if item["title"]:
                title = item["title"]
            else:
                title = "无标题"
            if item["topic_id"]:
                topic_id = item["topic_id"]
            else:
                topic_id = "0000000000"
            if item["visit_count"]:
                visit_count = item["visit_count"]
            else:
                visit_count = 0
            if item["follower_count"]:
                follower_count = item["follower_count"]
            else:
                follower_count = 0
            if item["answer_count"]:
                answer_count = item["answer_count"]
            else:
                answer_count = 0
            if item["author"]:
                author = item["author"]
            else:
                author = "未知"
            if item["content"]:
                content = item["content"]
            else:
                content = "无"
            try:
                updated_time = self.timestamp_tran(item["updated_time"])
            except Exception as err:
                updated_time = "1900-01-01"
            print(">>>>>>>>>>>>>>持久化文章信息：[ID=" + str(xid) + ", TITLE=" + title + "]")
            ArticleInfoDao.save(xid, title, topic_id, visit_count, follower_count, answer_count, author,
                                content, updated_time)
        elif isinstance(item, SpiderReplyItem):
            # 保存回答信息
            xid, article_id, comment_count, author, update_time = None, None, None, None, None
            voteup_count, content = None, None
            if item["id"]:
                xid = item["id"]
            else:
                xid = "0000000000"
            if item["article_id"]:
                article_id = item["article_id"]
            else:
                article_id = "0000000000"
            if item["comment_count"]:
                comment_count = item["comment_count"]
            else:
                comment_count = 0
            if item["author"]:
                author = item["author"]
            else:
                author = "未知"
            try:
                update_time = self.timestamp_tran(item["update_time"])
            except Exception as err:
                update_time = "1900-01-01"
            if item["voteup_count"]:
                voteup_count = item["voteup_count"]
            else:
                voteup_count = 0
            if item["content"]:
                content = item["content"]
            else:
                content = "无"
            print(">>>>>>>>>>>>>>持久化回答信息：[ID=" + str(xid) + ", AUTHOR=" + author + ", article_id=" + article_id + "]")
            ReplyInfoDao.save(xid, article_id, comment_count, author, update_time, voteup_count, content)

    def timestamp_tran(self, timeStamp):
        timearr = time.localtime(timeStamp)
        return time.strftime("%Y-%m-%d %H:%M:%S", timearr)