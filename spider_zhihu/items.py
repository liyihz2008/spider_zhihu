# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

'''
话题类别信息
'''


class SpiderTopicTypeItem(scrapy.Item):
    id = scrapy.Field()
    name = scrapy.Field()


'''
话题信息
'''


class SpiderTopicItem(scrapy.Item):
    id = scrapy.Field()
    name = scrapy.Field()
    type_id = scrapy.Field()
    href = scrapy.Field()


'''
话题列表的文章简要基础信息
'''


class SpiderArticleSimpleItem(scrapy.Item):
    id = scrapy.Field()
    topic_id = scrapy.Field()
    data_score = scrapy.Field()
    href = scrapy.Field()


'''
话题中的文章信息：可能是专栏文章可能是问题
'''


class SpiderArticleItem(scrapy.Item):
    id = scrapy.Field()
    topic_id = scrapy.Field()
    # article_type：QUESTION | ARTICLE
    article_type = scrapy.Field()
    title = scrapy.Field()
    visit_count = scrapy.Field()
    follower_count = scrapy.Field()
    answer_count = scrapy.Field()
    author = scrapy.Field()
    updated_time = scrapy.Field()
    content = scrapy.Field()


'''
话题中的文章的回复信息：可能是回答，可能是评论
'''


class SpiderReplyItem(scrapy.Item):
    id = scrapy.Field()
    article_id = scrapy.Field()
    comment_count = scrapy.Field()
    author = scrapy.Field()
    update_time = scrapy.Field()
    voteup_count = scrapy.Field()
    content = scrapy.Field()

