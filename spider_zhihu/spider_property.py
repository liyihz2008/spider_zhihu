class SpiderProperty:

    # 登录页URL地址
    signin_page_url = "https://www.zhihu.com/#signin"

    # 登录认证的POST请求地址
    login_post_url = 'https://www.zhihu.com/login/phone_num'

    # 验证码请求地址，后续需要加上时间缀
    captcha_url = "https://www.zhihu.com/captcha.gif?type=login&lang=cn"

    # 话题广场页面URL地址
    topic_type_url = "https://www.zhihu.com/topics"

    # 话题下一页请求地址
    topics_url = "https://www.zhihu.com/node/TopicsPlazzaListV2"

    # 话题文章列表
    topic_hot_url = "https://www.zhihu.com/topic/{toipc_id}/hot"

    # 文章具体信息
    artic_question_url = "https://www.zhihu.com"

    # 话题爬取最大页数
    topic_max_page = 10

    # 文章爬取最大页数
    article_max_page = 10

    # 回答爬取最大页数
    reply_max_page = 1
