# -*- coding: utf-8 -*-
import time


class Logger:

    def __init__(self):
        pass

    '''
    错误日志记录方法
    '''

    @classmethod
    def error_log(cls, message, err):
        log_fullname = "../zhihu_error.log"
        with open(log_fullname, 'a', encoding='utf-8', errors='ignore') as fh:
            try:
                fh.write(time.strftime("-" * 30 + "%Y-%m-%d %H:%M:%S", time.localtime(time.time())) + "-" * 30 + "\n")
                fh.write(message + ", Exception:" + str(err).encode('utf-8').decode('unicode_escape') + "\n")  # 写入信息需要转码
            except Exception as err:
                print("记录日志信息时发生异常:" + str(err))