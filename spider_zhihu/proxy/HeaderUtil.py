# -*- coding: utf-8 -*-
import random


class HeaderUtil:

    agent_pool = ["User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
                  "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36"]

    def __init__(self):
        pass

    @classmethod
    def get_login_request_header(cls):
        agent = random.choice(cls.agent_pool)
        headers = {
            "User-Agent": agent,
            "Host": "www.zhihu.com",
            "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
            "Accept-Encoding": "gzip, deflate, br",
            "Referer": "https://www.zhihu.com",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Cache-Control": "max-age=0",
            "Connection": "keep-alive"
        }
        return headers

    @classmethod
    def get_common_request_header(cls):
        agent = random.choice(cls.agent_pool)
        headers = {
            "User-Agent": agent,
            "Host": "www.zhihu.com",
            "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
            "Accept-Encoding": "gzip, deflate, br",
            "Referer": "https://www.zhihu.com/topics",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
        }
        return headers

    @classmethod
    def get_topic_request_header(cls, xsrftoken):
        agent = random.choice(cls.agent_pool)
        if xsrftoken and len(xsrftoken) > 0:
            headers = {
                "User-Agent": agent,
                "Host": "www.zhihu.com",
                "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
                "Accept-Encoding": "gzip, deflate, br",
                "Referer": "https://www.zhihu.com/topics",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                "Cache-Control": "no-cache",
                "Connection": "keep-alive",
                "Origin": "https://www.zhihu.com",
                "X-Requested-With": "XMLHttpRequest",
                "X-Xsrftoken": xsrftoken,
            }
        else:
            headers = {
                "User-Agent": agent,
                "Host": "www.zhihu.com",
                "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
                "Accept-Encoding": "gzip, deflate, br",
                "Referer": "https://www.zhihu.com/topics",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                "Cache-Control": "no-cache",
                "Connection": "keep-alive",
                "Origin": "https://www.zhihu.com",
                "X-Requested-With": "XMLHttpRequest",
            }
        return headers

    @classmethod
    def get_answer_request_header(cls, referer):
        agent = random.choice(cls.agent_pool)
        headers = {
            "Accept": "text/html,application/xhtml+xml,image/jxr,*/*",
            "Accept-Encoding": "gzip,deflate,br",
            "Accept-Language": "zh-CN",
            "Host": "www.zhihu.com",
            "User-Agent": agent,
            "Referer": referer,
            "Connection": "keep-alive"
        }
        return headers

