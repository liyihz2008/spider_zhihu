# -*- coding: utf-8 -*-
import requests
from lxml import etree


class IpProxy:

    def __init__(self):
        pass

    @classmethod
    def set_request_header(cls):
        header_accept = ("Accept", "text/html, application/xhtml+xml, image/jxr, */*")
        header_accept_encoding = ("Accept-Encoding", "gzip, deflate")
        header_accept_language = ("Accept-Language", "zh-CN")
        header_connection = ("Connection", "keep-alive")
        header_host = ("Host", "www.xicidaili.com")
        header_if_none_match = ("If-None-Match", "W/\"2386718d8ef4ce605dbe924c96ff2570\"")
        header_user_agent = ("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko")
        addheaders = [header_accept, header_accept_encoding, header_accept_language, header_connection,
                             header_host, header_if_none_match, header_user_agent]
        return addheaders
    '''
    获取动态IP的方法：尝试去请求动态代码IP的页面， 获取动态IP列表
    从Redis服务器里获取随机IP
    '''

    @classmethod
    def get_ip_pool(cls):
        print(">>>>>>>>>>>>>>>>>IpProxy正在初始化IP代理池......")
        pool = []
        headers = cls.set_request_header()
        session = requests.Session()
        session.headers.update(headers)
        response = session.get('http://www.xicidaili.com/')
        html = str(response.content, 'utf-8')
        root = etree.HTML(html)
        items = root.xpath("//tr")
        for item in items:
            try:
                td_list = item.xpath("td")
                if td_list and len(td_list) == 8:
                    ip = td_list[1].xpath('./text()')
                    port = td_list[2].xpath('./text()')
                    ip_port = ip[0]+":"+port[0]
                    pool.append(ip_port)
            except Exception as err:
                print(err)
                pass
        return pool

if  __name__ == '__main__':
    IpProxy.getIp()
