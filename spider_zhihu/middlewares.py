# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/spider-middleware.html

import random
from scrapy import signals
from scrapy.downloadermiddlewares.useragent import UserAgentMiddleware
from scrapy.contrib.downloadermiddleware.httpproxy import HttpProxyMiddleware

class SpiderZhihuSpiderMiddleware(object):

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Response, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


'''
添加了一个Middleware来专门用于设置请求的User-Agent和Referer
User-Agent从settings.py中的ROTATE_USER_AGENT属性中获取，随机抽取一个使用
'''


class RotateUserAgentMiddleware(UserAgentMiddleware):
    def __init__(self, user_agent):
        self.user_agent = user_agent

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            user_agent=crawler.settings.get('ROTATE_USER_AGENT')
        )

    def process_request(self, request, spider):
        agent = random.choice(self.user_agent)
        request.headers['User-Agent'] = agent
        request.headers['Host'] = "www.zhihu.com"
        request.headers['Referer'] = "https://www.zhihu.com/topics"
        request.headers['Accept-Encoding'] = "gzip, deflate"
        request.headers['Accept-Language'] = "zh-CN"
        request.headers['Cache-Control'] = "no-cache"
        request.headers['Connection'] = "Keep-Alive"
        request.headers['Content-Type'] = "application/x-www-form-urlencoded; charset=UTF-8"
        request.headers['Accept'] = "*/*"

'''
动态设置ip代理（暂时不用了）
'''


class RotateIpProxyMiddleware(HttpProxyMiddleware):

    def process_request(self, request, spider):
          #if spider.ip_pool == None or len(spider.ip_pool) == 0:
          #    print(">>>>>>>>>>>>>>>>>在RotateIpProxyMiddleware中初始化IP代理池......")
              #spider.ip_pool = IpProxy.get_ip_pool()
          #radom_ip = random.choice(spider.ip_pool)
          radom_ip = "139.196.202.164:9001"
          print(">>>>>>>>>>>>>>>>>当前使用的IP地址：" + radom_ip)
          request.meta["proxy"] = radom_ip