# -*- coding: utf-8 -*-
from spider_zhihu.dao.article_info_dao import ArticleInfoDao
from spider_zhihu.service.article_data_service import ArticleDataStateJsonService
from spider_zhihu.items import SpiderArticleItem
from spider_zhihu.items import SpiderArticleSimpleItem


class ArticleInfoService:

    def __init__(self):
        pass

    '''
    查询文章是否已经存在了（分布式可以在redis里查询）
    '''

    @classmethod
    def exists(cls, article_id):
        article = ArticleInfoDao.get_article_info(article_id)
        if article:
            return True
        else:
            return False

    '''
    从topic_feed_list元素列表组织一个文章信息对象列表（简要信息对象）
    '''

    @classmethod
    def compose_article_from_feed_list(cls, topic_feed_list):
        item_array = []
        if topic_feed_list and len(topic_feed_list) > 0:
            for topic_feed in topic_feed_list:
                item = SpiderArticleSimpleItem()
                data_score = topic_feed.xpath("@data-score").extract()
                href = topic_feed.xpath(".//a[@class='question_link']/@href").extract()
                if href and len(href) > 0:
                    item["id"] = href[0].replace("/question/", "")
                item["data_score"] = data_score
                item["href"] = href
                item_array.append(item)
        return item_array

    '''
    从JSON数据中获取信息，组织一个文章信息对象
     '''

    @classmethod
    def compose_article_from_data_state(cls, topic_id, article_id, response_json):
        if response_json:
            item = SpiderArticleItem()
            item["id"] = article_id
            item["topic_id"] = topic_id
            item["article_type"] = "QUESTION"
            item["content"] = ArticleDataStateJsonService.get_detail(article_id, response_json)
            item["title"] = ArticleDataStateJsonService.get_title(article_id, response_json)
            item["visit_count"] = ArticleDataStateJsonService.get_visit_count(article_id, response_json)
            item["follower_count"] = ArticleDataStateJsonService.get_follower_count(article_id, response_json)
            item["answer_count"] = ArticleDataStateJsonService.get_answer_count(article_id, response_json)
            item["author"] = ArticleDataStateJsonService.get_author(article_id, response_json)
            item["updated_time"] = ArticleDataStateJsonService.get_update_time(article_id, response_json)
            return item

    '''
    将数组拼接为字符串，丢弃非字符串对象
    '''

    @classmethod
    def join_array_to_string(cls, message_list):
        item_html = ""
        for msg in message_list:
            if isinstance(msg, str):
                item_html += msg
        return item_html

    '''
    请求文章列表下一页的post_data组织
    '''

    @classmethod
    def compose_article_list_post_data(cls, last_data_score):
        post_data = {
            "start": "0",
            "offset": str(last_data_score)
        }
        return post_data



