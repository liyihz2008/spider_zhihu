# -*- coding: utf-8 -*-
import json
from datetime import datetime


class ArticleDataStateJsonService:

    def __init__(self):
        pass

    @classmethod
    def tran_string_to_date(cls, content):
        return datetime.strptime(content, '%Y-%m-%d %H:%M:%S')

    '''
    将字符串转化为JSON对象，注意，有一些转义字符需要处理
     '''

    @classmethod
    def parse_to_json(cls, data_state_content):
        data_state_content = data_state_content.replace("&quot;", '"').replace("&lt;", '<').replace("&gt;", '>')
        return json.loads(data_state_content)

    @classmethod
    def tran_tag_in_content(cls, content):
        content = content.replace('"', "&quot;").replace('<', "&lt;").replace('>', "&gt;")
        return content

    '''
    从JSON中获取文章全部内容
    '''

    @classmethod
    def get_detail(cls, article_id, response_json):
        entities = response_json["entities"]
        if entities:
            questions = entities["questions"]
            if questions:
                question = questions[article_id]
                if question:
                    return question["editableDetail"]

    '''
    从JSON中获取标题信息
    '''

    @classmethod
    def get_title(cls, article_id, response_json):
        entities = response_json["entities"]
        if entities:
            questions = entities["questions"]
            if questions:
                question = questions[article_id]
                if question:
                    return question["title"]

    '''
    从JSON中获取作者信息
    '''

    @classmethod
    def get_author(cls, article_id, response_json):
        entities = response_json["entities"]
        if entities:
            questions = entities["questions"]
            if questions:
                question = questions[article_id]
                if question:
                    author = question["author"]
                    if author:
                        return author["name"]

    '''
    从JSON中获取访问次数
    '''

    @classmethod
    def get_visit_count(cls, article_id, response_json):
        entities = response_json["entities"]
        if entities:
            questions = entities["questions"]
            if questions:
                question = questions[article_id]
                if question:
                    return question["visitCount"]

    '''
    从JSON中获取关注者数量
    '''

    @classmethod
    def get_follower_count(cls, article_id, response_json):
        entities = response_json["entities"]
        if entities:
            questions = entities["questions"]
            if questions:
                question = questions[article_id]
                if question:
                    return question["followerCount"]

    '''
    从JSON中获取回复总数
    '''

    @classmethod
    def get_answer_count(cls, article_id, response_json):
        entities = response_json["entities"]
        if entities:
            questions = entities["questions"]
            if questions:
                question = questions[article_id]
                if question:
                    return question["answerCount"]

    '''
    从JSON中获取回复下一次URL
    '''

    @classmethod
    def get_answer_next_page_url(cls, article_id, response_json):
        question = response_json["question"]
        if question:
            answers = question["answers"]
            if answers:
                article = answers[article_id]
                if article:
                    return article["next"]

    '''
        从JSON中获取更新时间
        '''

    @classmethod
    def get_update_time(cls, article_id, response_json):
        entities = response_json["entities"]
        if entities:
            questions = entities["questions"]
            if questions:
                question = questions[article_id]
                if question:
                    return question["updatedTime"]

