# -*- coding: utf-8 -*-
from spider_zhihu.items import SpiderTopicTypeItem
from spider_zhihu.items import SpiderTopicItem


class TopicService:

    def __init__(self):
        pass

    '''
    根据传入的数据组织一个登录请求的Post数据包
    '''

    @classmethod
    def compose_post_data(cls, page_num, topic_id):
        post_data = {
            "method": "next",
            "params": '{"topic_id":' + str(topic_id) + ',"offset":' + str(page_num*20)
                      + ',"hash_id":"1c4d84cdb602654d0eb4cf42b1ba6397"}'
        }
        return post_data

    '''
    根据指定的HTML元素节点，获取信息组织一个item(类：SpiderTopicTypeItem)
    '''

    @classmethod
    def compose_topic_type_item(cls, li_node):
        data_id = li_node.xpath("@data-id").extract()
        type_name = li_node.xpath(".//a/text()").extract()
        item = SpiderTopicTypeItem()
        item["id"] = data_id
        item["name"] = type_name
        return item

    '''
    根据指定的HTML元素节点列表，获取信息组织一个item列表(类：SpiderTopicItem)
    '''
    @classmethod
    def list_topic_items(cls, topic_type_id, topic_item_a_list):
        item_array = []
        if topic_item_a_list and len(topic_item_a_list) > 0:
            for tag_a in topic_item_a_list:
                item = cls.compose_topic_item(topic_type_id, tag_a)
                item_array.append(item)
        return item_array

    '''
    根据指定的HTML元素节点，获取信息组织一个item(类：SpiderTopicItem)
    '''
    @classmethod
    def compose_topic_item(cls, topic_type_id, tag_a_node):
        link_array = tag_a_node.xpath("@href").extract()
        name_array = tag_a_node.xpath(".//strong/text()").extract()
        item = SpiderTopicItem()
        item["href"] = link_array
        item["name"] = name_array
        item["type_id"] = topic_type_id
        if link_array and len(link_array) > 0:
            topic_id = link_array[0].replace("/topic/", "")
            item["id"] = topic_id
        return item

