# -*- coding: utf-8 -*-
import json
import os


class LoginService:

    def __init__(self):
        pass

    '''
    获取_xsrf参数的值
    '''

    @classmethod
    def get_xsrf_token(cls, response):
        # 获取_xsrf的标签，可能有好几个，大概5个，值都一样，可以取第一个作为_xsrf值
        _xsrf = response.xpath(".//input[@name='_xsrf']/@value").extract()
        if _xsrf and len(_xsrf) > 0:
            return _xsrf[0]
        else:
            return None

    '''
    根据传入的数据组织一个登录请求的Post数据包
    '''

    @classmethod
    def compose_login_data(cls, captcha_type, xsrf_token, phone_num, password, captcha):
        post_data = {"captcha_type": captcha_type, "_xsrf": xsrf_token, "phone_num": phone_num, "password": password,
                     "captcha": captcha}
        return post_data;

    '''
    修改response里的指定数据包中的指定参数值
    '''

    @classmethod
    def set_login_data(cls, response, name, atrr_name, attr_value):
        post_data = response.meta.get(name, {})
        post_data[atrr_name] = attr_value
        return post_data;

    '''
    将验证码内容持久化到本地磁盘
    '''

    @classmethod
    def write_captcha(cls, file_name, content):
        # 保存验证码
        with open(file_name, 'wb') as f:
            f.write(content)
            f.close()
        captcha_path = os.path.abspath(os.path.join(os.getcwd(), "."))
        return captcha_path

    '''
    根据用户输入的数字，返回指定图片的坐标值
    '''

    @classmethod
    def get_captcha_choose(cls, input_numbers):
        # 输入验证码
        captcha = {
            'img_size': [200, 44],
            'input_points': [],
        }
        points = [[22.796875, 22],
                  [42.796875, 22],
                  [63.796875, 21],
                  [84.796875, 20],
                  [107.796875, 20],
                  [129.796875, 22],
                  [150.796875, 22]]
        for num in input_numbers:
            captcha['input_points'].append(points[int(num) - 1])
        return json.dumps(captcha)

    '''
    根据响应的内容，判断是否已经登录成功
    '''

    @classmethod
    def is_login_success(cls, response):
        text_json = json.loads(response.text)
        if 'msg' in text_json and text_json['msg'] == '登录成功':
            return True
        else:
            print(text_json)
            return False

    '''
    该方法没有实际用途，仅仅为了好看，打印一些分隔线  
    '''

    @classmethod
    def print_lines(cls, captcha_path):
        print("")
        print("")
        print("=======================================================================================================")
        print("-------------------------------------------------------------------------------------------------------")
        print("")
        print(">>>>>>>>>>>>>验证码图片保存位置：" + captcha_path + '/captcha.gif')
        print("")
        print(">>>>>>>>>>>>>注意：如果有多个位置，请连续输入多个数字（从1开始）不需要分隔符。")
        print("")
        print("-------------------------------------------------------------------------------------------------------")
        print("=======================================================================================================")