# -*- coding: utf-8 -*-
from spider_zhihu.service.answer_data_service import AnswerDataStateJsonService
from spider_zhihu.items import SpiderReplyItem


class AnswerInfoService:

    def __init__(self):
        pass

    '''
    将JSON组织成一个回答对象item列表(类：SpiderReplyItem)
     '''

    @classmethod
    def compose_answers_from_data_state(cls, article_id, response_json):
        ids = AnswerDataStateJsonService.get_answer_ids(article_id, response_json)
        answers_json_list = AnswerDataStateJsonService.get_answer_list(ids, response_json)
        return cls.compose_answers_from_json_list(article_id, answers_json_list)

    '''
    将回复JSON数组组织成一个回答对象item列表(类：SpiderReplyItem)
    '''

    @classmethod
    def compose_answers_from_json_list(cls, article_id, answers_json_list):
        item_array = []
        if answers_json_list and len(answers_json_list) > 0:
            for answer_json in answers_json_list:
                item = SpiderReplyItem()
                item["article_id"] = article_id
                item["id"] = AnswerDataStateJsonService.get_id(answer_json)
                item["update_time"] = AnswerDataStateJsonService.get_update_time(answer_json)
                item["author"] = AnswerDataStateJsonService.get_author(answer_json)
                item["comment_count"] = AnswerDataStateJsonService.get_comment_count(answer_json)
                item["content"] = AnswerDataStateJsonService.get_detail(answer_json)
                item["voteup_count"] = AnswerDataStateJsonService.get_voteup_count(answer_json)
                item_array.append(item)
            return item_array

    '''
        从下一页回复JSON中获取下一页的请求URL地址
        '''

    @classmethod
    def get_next_url_from_paging(cls, paging):
        if paging:
            if paging["is_end"]:
                return None
            else:
                return paging["next"]


