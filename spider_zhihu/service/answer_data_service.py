# -*- coding: utf-8 -*-
from datetime import datetime

class AnswerDataStateJsonService:

    def __init__(self):
        pass

    @classmethod
    def tran_string_to_date(cls, content):
        return datetime.strptime(content, '%Y-%m-%d %H:%M:%S')

    @classmethod
    def tran_tag_in_content(cls, content):
        content = content.replace('"', "&quot;").replace('<', "&lt;").replace('>', "&gt;")
        return content

    '''
    从文章响应的JSON中获取回复ID列表[String]
    question.answers.article_id.ids
    '''

    @classmethod
    def get_answer_ids(cls, article_id, response_json):
        question = response_json["question"]
        if question:
            answers = question["answers"]
            if answers:
                article = answers[article_id]
                if article:
                    return article["ids"]

    '''
    从响应JSON中获取回复列表JSON对象answer_json，需要先获取到所有的回答的ID列表才能获取到
    '''

    @classmethod
    def get_answer_list(cls, ids, response_json):
        answers_array = []
        entities = response_json["entities"]
        if entities:
            answers = entities["answers"]
            if ids and len(ids) > 0:
                for xid in ids:
                    answers_array.append(answers[str(xid)])
        return answers_array

    '''
        answer_json中获取回复全部内容
        '''

    @classmethod
    def get_id(cls, answer_json):
        if answer_json:
            return answer_json["id"]

    '''
    answer_json中获取回复全部内容
    '''

    @classmethod
    def get_detail(cls, answer_json):
        if answer_json:
            return answer_json["content"]

    '''
    从answer_json中获取回复作者信息
    '''

    @classmethod
    def get_author(cls, answer_json):
        if answer_json:
            author = answer_json["author"]
            if author:
                return author["name"]

    '''
    从answer_json中获取回复评论数量
    知乎会根据访问的时间随机变更Json中变更的名称，这里需要替换一下，比较贱，试了好久
    '''

    @classmethod
    def get_comment_count(cls, answer_json):
        if answer_json:
            try:
                return answer_json["comment_count"]
            except Exception as err:
                return answer_json["commentCount"]



    '''
    从answer_json中获取回复被顶的次数
    知乎会根据访问的时间随机变更Json中变更的名称，这里需要替换一下，比较贱，试了好久
    '''

    @classmethod
    def get_voteup_count(cls, answer_json):
        if answer_json:
            try:
                return answer_json["voteup_count"]
            except Exception as err:
                return answer_json["voteupCount"]

    '''
    从answer_json中获取回复更新时间
    知乎会根据访问的时间随机变更Json中变更的名称，这里需要替换一下，比较贱，试了好久
    '''

    @classmethod
    def get_update_time(cls, answer_json):
        if answer_json:
            try:
                return answer_json["updated_time"]
            except Exception as err:
                return answer_json["updatedTime"]

    '''
    从JSON中获取回复下一次URL
    '''

    @classmethod
    def get_answer_next_page_url(cls, article_id, response_json):
        question = response_json["question"]
        if question:
            answers = question["answers"]
            if answers:
                article = answers[article_id]
                if article:
                    return article["next"]
