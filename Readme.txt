说明：

1、暂时没有时间去弄分布式，先把单机版的整理好

2、数据结构见《数据结构设计》

3、项目分了几层
   dao - 主要是数据库操作类
   service - 主要是对象的服务类
   proxy - 主要是HTTPHEADER的组装，IP池的管理（IP池暂时没有使用middlewares.py里有一点实现，但是配置里注释掉了）

   --其他类的说明：
   dbm.py - 数据库配置
   db_connection.py  数据库连接管理（数据库连接的获取、释放）
   spider_property.py 是所有全局变量的管理，主要是集中管理一些URL，免得到爬虫代码里改
   另外，发现python3.6下面没有好的数据库连接池的实现，有空得自己写一套

4、不用登录也可以爬取数据，只是回答的数据只有一页，如果登录后就可以爬取下一页了
   使不使用登录可以在spider_zhihutopic.py类里使用need_login = False来进行控制，作了个简单的判断而已


另外，经过分析后发现，文章信息和回答信息的数据不需要在HTML上进行分析，只需要拿到一个DIV，解析给出的JSON数据即可
data_state = response.xpath(".//div[@id='data']/@data-state").extract()
回答的下一页的URL也在其中，格式请见文件：
    article_json.json - 文章详细页面拿到的json
    answer-page2.json - 请求回答的下一页拿到的json, 如果没有数据了，data为空，可以作为判断依据


